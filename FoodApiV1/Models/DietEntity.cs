﻿namespace FoodApiV1.Models
{
    public class DietEntity
    {
        public int Id { get; set; }
        public string ContainIds { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Diet description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// URL of the diet's card image
        /// </summary>
        public string ImageUrl { get; set; }

    }
}