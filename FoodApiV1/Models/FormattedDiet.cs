﻿using System.Collections.Generic;
using FoodApiV1.number2;

namespace FoodApiV1.Models
{
    public class FormattedDiet
    {
        public FormattedDiet()
        {
            DietPlan = new List<FormattedDietPlan>();
        }
        
        public int DietId { get; set; }
        public string DietName { get; set; }
        public string DietDescription { get; set; }
        public string Preview { get; set; }
        public List<FormattedDietPlan> DietPlan { get; set; }
    }
}