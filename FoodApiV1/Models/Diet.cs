﻿using System.Collections.Generic;

namespace FoodApiV1.Models
{
    public class Diet
    {
        public int Id { get; set; }
        /// <summary>
        /// Diet name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Diet description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// URL of the diet's card image
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// Products of which diet is consisted of
        /// </summary>
        public ICollection<FoodDesFormatted> Components { get; set; }
    }
}