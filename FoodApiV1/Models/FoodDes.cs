﻿using System;
using System.Collections.Generic;

namespace FoodApiV1.Models
{
    public partial class FoodDes
    {
        public FoodDes()
        {
            Datsrcln = new HashSet<Datsrcln>();
            Langual = new HashSet<Langual>();
            NutData = new HashSet<NutData>();
            Weight = new HashSet<Weight>();
        }

        public string NdbNo { get; set; }
        public string FdGrpCd { get; set; }
        public string LongDesc { get; set; }
        public string ShrtDesc { get; set; }
        public string ComName { get; set; }
        public string ManufacName { get; set; }
        public string Survey { get; set; }
        public string RefDesc { get; set; }
        public decimal? Refuse { get; set; }
        public string SciName { get; set; }
        public decimal? NFactor { get; set; }
        public decimal? ProFactor { get; set; }
        public decimal? FatFactor { get; set; }
        public decimal? ChoFactor { get; set; }

        public virtual FdGroup FdGrpCdNavigation { get; set; }
        public virtual ICollection<Datsrcln> Datsrcln { get; set; }
        public virtual ICollection<Langual> Langual { get; set; }
        public virtual ICollection<NutData> NutData { get; set; }
        public virtual ICollection<Weight> Weight { get; set; }

    }
}
