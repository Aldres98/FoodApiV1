﻿namespace FoodApiV1.Models
{
    /// <summary>
    /// Model for representing nutrient in a human-readable form
    /// (existing form from usdanlsr28 database in not convenient)
    /// </summary>
    public class ReadableNutrient
    {
        
        /// <summary>
        /// Name of the nutrient
        /// </summary>
        public string NutrientName { get; set; }
        /// <summary>
        /// Quantity of the nutrient per 100 grams
        /// </summary>
        public decimal NutrientValue { get; set; }
    }
}