﻿namespace FoodApiV1.Models
{
    public class FormattedDish
    {
        public int DishId { get; set; }
        public string DishName { get; set; }
        public string DishDescription { get; set; }
        public string IngredientsList { get; set; }
        public string Directions { get; set; }
        public int Kcal { get; set; }
        public int Protein { get; set; } 
        public int Fat { get; set; }
        public int Carbs { get; set; }
        public string PicsUrl { get; set; }
        
    }
}