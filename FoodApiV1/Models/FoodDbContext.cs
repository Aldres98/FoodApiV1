﻿using System;
using FoodApiV1.Models2;
using FoodApiV1.number2;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FoodApiV1.Models
{
    public partial class FoodDbContext : DbContext
    {
        public FoodDbContext()
        {
        }

        public FoodDbContext(DbContextOptions<FoodDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DataSrc> DataSrc { get; set; }
        public virtual DbSet<Datsrcln> Datsrcln { get; set; }
        public virtual DbSet<DerivCd> DerivCd { get; set; }
        public virtual DbSet<FdGroup> FdGroup { get; set; }
        public virtual DbSet<FoodDes> FoodDes { get; set; }
        public virtual DbSet<Footnote> Footnote { get; set; }
        public virtual DbSet<Langdesc> Langdesc { get; set; }
        public virtual DbSet<Langual> Langual { get; set; }
        public virtual DbSet<NutData> NutData { get; set; }
        public virtual DbSet<NutrDef> NutrDef { get; set; }
        public virtual DbSet<SrcCd> SrcCd { get; set; }
        public virtual DbSet<Weight> Weight { get; set; }
        public virtual DbSet<CustomDish> CustomDishes { get; set; }
        public virtual DbSet<DietEntity> DietEntities { get; set; }
        public virtual DbSet<PreparedDiets> PreparedDiets { get; set; }
        public virtual DbSet<PreparedDishes> PreparedDishes { get; set; }
        public virtual DbSet<DietPlan> DietPlan { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=40.118.66.25; Database=usdanlsr28;User=dietka;Password=dietkateam;", x => x.ServerVersion("10.5.5-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<CustomDishItem>().HasKey(i => new { i.FoodDesId, i.CustomDishId });

            modelBuilder.Entity<DataSrc>(entity =>
            {
                entity.ToTable("data_src");

                entity.Property(e => e.DataSrcId)
                    .HasColumnName("DataSrc_ID")
                    .HasColumnType("char(6)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Authors)
                    .HasColumnType("char(255)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.EndPage)
                    .HasColumnName("End_Page")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IssueState)
                    .HasColumnName("Issue_State")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Journal)
                    .HasColumnType("char(135)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.StartPage)
                    .HasColumnName("Start_Page")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnType("char(255)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.VolCity)
                    .HasColumnName("Vol_City")
                    .HasColumnType("char(16)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Year)
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });
            
            modelBuilder.Entity<DietPlan>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("diet_plan");

                entity.Property(e => e.DayNumber)
                    .HasColumnName("day_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DietId)
                    .HasColumnName("diet_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DishId)
                    .HasColumnName("dish_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Meal)
                    .HasColumnName("meal")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });



            modelBuilder.Entity<Datsrcln>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.NutrNo, e.DataSrcId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("datsrcln");

                entity.HasIndex(e => e.DataSrcId)
                    .HasName("DATSRCLN_SRC_FK");

                entity.HasIndex(e => e.NutrNo)
                    .HasName("DATSRCLN_NUT_FK");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DataSrcId)
                    .HasColumnName("DataSrc_ID")
                    .HasColumnType("char(6)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.DataSrc)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.DataSrcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DATSRCLN_SRC_FK");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DATSRCLN_NDB_FK");

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DATSRCLN_NUT_FK");
            });

            modelBuilder.Entity<DerivCd>(entity =>
            {
                entity.HasKey(e => e.DerivCd1)
                    .HasName("PRIMARY");

                entity.ToTable("deriv_cd");

                entity.Property(e => e.DerivCd1)
                    .HasColumnName("Deriv_Cd")
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DerivDesc)
                    .IsRequired()
                    .HasColumnName("Deriv_Desc")
                    .HasColumnType("char(120)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<FdGroup>(entity =>
            {
                entity.HasKey(e => e.FdGrpCd)
                    .HasName("PRIMARY");

                entity.ToTable("fd_group");

                entity.Property(e => e.FdGrpCd)
                    .HasColumnName("FdGrp_Cd")
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FdGrpDesc)
                    .IsRequired()
                    .HasColumnName("FdGrp_Desc")
                    .HasColumnType("char(60)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<FoodDes>(entity =>
            {
                entity.HasKey(e => e.NdbNo)
                    .HasName("PRIMARY");

                entity.ToTable("food_des");

                entity.HasIndex(e => e.FdGrpCd)
                    .HasName("FOOD_DES_FK");

                entity.HasIndex(e => new { e.NdbNo, e.FdGrpCd })
                    .HasName("FOOD_DES_UK")
                    .IsUnique();

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ChoFactor)
                    .HasColumnName("CHO_Factor")
                    .HasColumnType("decimal(4,2)");

                entity.Property(e => e.ComName)
                    .HasColumnType("char(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FatFactor)
                    .HasColumnName("Fat_Factor")
                    .HasColumnType("decimal(4,2)");

                entity.Property(e => e.FdGrpCd)
                    .IsRequired()
                    .HasColumnName("FdGrp_Cd")
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.LongDesc)
                    .IsRequired()
                    .HasColumnName("Long_Desc")
                    .HasColumnType("char(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ManufacName)
                    .HasColumnType("char(65)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NFactor)
                    .HasColumnName("N_Factor")
                    .HasColumnType("decimal(4,2)");

                entity.Property(e => e.ProFactor)
                    .HasColumnName("Pro_Factor")
                    .HasColumnType("decimal(4,2)");

                entity.Property(e => e.RefDesc)
                    .HasColumnName("Ref_desc")
                    .HasColumnType("char(135)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Refuse).HasColumnType("decimal(2,0)");

                entity.Property(e => e.SciName)
                    .HasColumnType("char(65)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ShrtDesc)
                    .IsRequired()
                    .HasColumnName("Shrt_Desc")
                    .HasColumnType("char(60)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Survey)
                    .HasColumnType("char(1)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.FdGrpCdNavigation)
                    .WithMany(p => p.FoodDes)
                    .HasForeignKey(d => d.FdGrpCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FOOD_DES_FK");
            });

            modelBuilder.Entity<Footnote>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("footnote");

                entity.Property(e => e.FootNtNo)
                    .IsRequired()
                    .HasColumnName("FootNt_No")
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FootntTxt)
                    .IsRequired()
                    .HasColumnName("Footnt_Txt")
                    .HasColumnType("char(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FootntTyp)
                    .IsRequired()
                    .HasColumnName("Footnt_Typ")
                    .HasColumnType("char(1)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NdbNo)
                    .IsRequired()
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Langdesc>(entity =>
            {
                entity.HasKey(e => e.FactorCode)
                    .HasName("PRIMARY");

                entity.ToTable("langdesc");

                entity.Property(e => e.FactorCode)
                    .HasColumnName("Factor_Code")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("char(140)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<Langual>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.FactorCode })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("langual");

                entity.HasIndex(e => e.FactorCode)
                    .HasName("LANGUAL_LANGDESC_FK");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FactorCode)
                    .HasColumnName("Factor_Code")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.FactorCodeNavigation)
                    .WithMany(p => p.Langual)
                    .HasForeignKey(d => d.FactorCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LANGUAL_LANGDESC_FK");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Langual)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LANGUAL_FOOD_DES_FK");
            });

            modelBuilder.Entity<NutData>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.NutrNo })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("nut_data");

                entity.HasIndex(e => e.NutrNo)
                    .HasName("NUT_DATA_NUTDEF_FK");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.AddModDate)
                    .HasColumnName("AddMod_Date")
                    .HasColumnType("char(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.AddNutrMark)
                    .HasColumnName("Add_Nutr_Mark")
                    .HasColumnType("char(1)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cc)
                    .HasColumnName("CC")
                    .HasColumnType("char(1)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DerivCd)
                    .HasColumnName("Deriv_Cd")
                    .HasColumnType("char(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Df)
                    .HasColumnName("DF")
                    .HasColumnType("decimal(4,0)");

                entity.Property(e => e.LowEb)
                    .HasColumnName("Low_EB")
                    .HasColumnType("decimal(10,3)");

                entity.Property(e => e.Max).HasColumnType("decimal(10,3)");

                entity.Property(e => e.Min).HasColumnType("decimal(10,3)");

                entity.Property(e => e.NumDataPts)
                    .HasColumnName("Num_Data_Pts")
                    .HasColumnType("decimal(5,0)");

                entity.Property(e => e.NumStudies)
                    .HasColumnName("Num_Studies")
                    .HasColumnType("decimal(2,0)");

                entity.Property(e => e.NutrVal)
                    .HasColumnName("Nutr_Val")
                    .HasColumnType("decimal(10,3)");

                entity.Property(e => e.RefNdbNo)
                    .HasColumnName("Ref_NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.SrcCd)
                    .IsRequired()
                    .HasColumnName("Src_Cd")
                    .HasColumnType("char(2)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.StatCmt)
                    .HasColumnName("Stat_cmt")
                    .HasColumnType("char(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.StdError)
                    .HasColumnName("Std_Error")
                    .HasColumnType("decimal(8,3)");

                entity.Property(e => e.UpEb)
                    .HasColumnName("Up_EB")
                    .HasColumnType("decimal(10,3)");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("NUT_DATA_NDBNO_FK");

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("NUT_DATA_NUTDEF_FK");
            });

            modelBuilder.Entity<NutrDef>(entity =>
            {
                entity.HasKey(e => e.NutrNo)
                    .HasName("PRIMARY");

                entity.ToTable("nutr_def");

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NumDec)
                    .HasColumnName("Num_Dec")
                    .HasColumnType("decimal(6,0)");

                entity.Property(e => e.NutrDesc)
                    .IsRequired()
                    .HasColumnType("char(60)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.SrOrder)
                    .HasColumnName("SR_Order")
                    .HasColumnType("decimal(6,0)");

                entity.Property(e => e.Tagname)
                    .HasColumnType("char(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Units)
                    .IsRequired()
                    .HasColumnType("char(7)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<SrcCd>(entity =>
            {
                entity.HasKey(e => e.SrcCd1)
                    .HasName("PRIMARY");

                entity.ToTable("src_cd");

                entity.Property(e => e.SrcCd1)
                    .HasColumnName("Src_Cd")
                    .HasColumnType("char(2)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.SrcCdDesc)
                    .IsRequired()
                    .HasColumnName("SrcCd_Desc")
                    .HasColumnType("char(60)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });
            
                        modelBuilder.Entity<PreparedDiets>(entity =>
            {
                entity.HasKey(e => e.DietId)
                    .HasName("PRIMARY");

                entity.ToTable("prepared_diets");

                entity.Property(e => e.DietId)
                    .HasColumnName("diet_id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DietDescription)
                    .HasColumnName("diet_description")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DietName)
                    .HasColumnName("diet_name")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Preview)
                    .HasColumnName("preview")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<PreparedDishes>(entity =>
            {
                entity.HasKey(e => e.DishId)
                    .HasName("PRIMARY");

                entity.ToTable("prepared_dishes");

                entity.Property(e => e.DishId)
                    .HasColumnName("dish_id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Carbs)
                    .HasColumnName("carbs")
                    .HasColumnType("int(11)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Directions)
                    .HasColumnName("directions")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DishDescription)
                    .HasColumnName("dish_description")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DishName)
                    .HasColumnName("dish_name")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Fat)
                    .HasColumnName("fat")
                    .HasColumnType("int(11)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.IngredientList)
                    .HasColumnName("ingredient_list")
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Kcal)
                    .HasColumnName("kcal")
                    .HasColumnType("int(11)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PicsUrl)
                    .HasColumnName("pics_url")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Protein)
                    .HasColumnName("protein")
                    .HasColumnType("int(11)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });


            modelBuilder.Entity<Weight>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.Seq })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("weight");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasColumnType("char(5)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Seq)
                    .HasColumnType("char(2)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Amount).HasColumnType("decimal(5,3)");

                entity.Property(e => e.GmWgt)
                    .HasColumnName("Gm_Wgt")
                    .HasColumnType("decimal(7,1)");

                entity.Property(e => e.MsreDesc)
                    .IsRequired()
                    .HasColumnName("Msre_Desc")
                    .HasColumnType("char(84)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NumDataPts)
                    .HasColumnName("Num_Data_Pts")
                    .HasColumnType("decimal(3,0)");

                entity.Property(e => e.StdDev)
                    .HasColumnName("Std_Dev")
                    .HasColumnType("decimal(7,3)");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Weight)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WEIGHT_NDBNO_FK");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
