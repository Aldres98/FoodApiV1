﻿namespace FoodApiV1.Models
{
    public class FormattedDietPlan
    {
        public int DayNumber { get; set; }
        public string Meal { get; set; }
        public FormattedDish Dish { get; set; }
    }
}