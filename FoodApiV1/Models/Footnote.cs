﻿using System;
using System.Collections.Generic;

namespace FoodApiV1.Models
{
    public partial class Footnote
    {
        public string NdbNo { get; set; }
        public string FootNtNo { get; set; }
        public string FootntTyp { get; set; }
        public string NutrNo { get; set; }
        public string FootntTxt { get; set; }
    }
}
