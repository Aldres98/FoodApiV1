﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApiV1.Models
{
    public class CustomDish
    {
        
        public int ID { get; set; }
        
        /// <summary>
        /// List of all ingredients that dish contains
        /// </summary>
        /// <summary>
        /// Title of the custom dish
        /// </summary>
        public string Title { get; set; }
        [NotMapped]
        public ICollection<CustomDishItem> FoodDesCustomDishItem { get; set; }

    }
}