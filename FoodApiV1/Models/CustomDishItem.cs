﻿namespace FoodApiV1.Models
{
    public class CustomDishItem
    {
        public int FoodDesId { get; set; }
        public FoodDes FoodDes { get; set; }
        
        public int CustomDishId { get; set; }
        public CustomDish CustomDish { get; set; }
    }
}