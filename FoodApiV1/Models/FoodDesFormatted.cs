﻿using System.Collections.Generic;

namespace FoodApiV1.Models
{
    public class FoodDesFormatted
    {
        public FoodDesFormatted()
        {
            Nutrients = new List<ReadableNutrient>();
        }
        public FoodDes FoodDes { get; set; }
        public List<ReadableNutrient> Nutrients { get; set; }
    }
}