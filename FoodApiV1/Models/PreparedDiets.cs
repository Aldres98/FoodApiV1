﻿using System;
using System.Collections.Generic;

namespace FoodApiV1.Models2
{
    public partial class PreparedDiets
    {
        public int DietId { get; set; }
        public string DietName { get; set; }
        public string DietDescription { get; set; }
        public string Preview { get; set; }
    }
}
