﻿using System;
using System.Collections.Generic;

namespace FoodApiV1.Models
{
    public partial class SrcCd
    {
        public string SrcCd1 { get; set; }
        public string SrcCdDesc { get; set; }
    }
}
