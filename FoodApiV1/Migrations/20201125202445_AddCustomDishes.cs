﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodApiV1.Migrations
{
    public partial class AddCustomDishes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            

            migrationBuilder.CreateTable(
                name: "CustomDishItem",
                columns: table => new
                {
                    FoodDesId = table.Column<int>(nullable: false),
                    CustomDishId = table.Column<int>(nullable: false),
                    FoodDesNdbNo = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomDishItem", x => new { x.FoodDesId, x.CustomDishId });
                    table.ForeignKey(
                        name: "FK_CustomDishItem_CustomDishes_CustomDishId",
                        column: x => x.CustomDishId,
                        principalTable: "CustomDishes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomDishItem_food_des_FoodDesNdbNo",
                        column: x => x.FoodDesNdbNo,
                        principalTable: "food_des",
                        principalColumn: "NDB_No",
                        onDelete: ReferentialAction.Cascade);
                });



            migrationBuilder.CreateIndex(
                name: "IX_CustomDishItem_CustomDishId",
                table: "CustomDishItem",
                column: "CustomDishId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomDishItem_FoodDesNdbNo",
                table: "CustomDishItem",
                column: "FoodDesNdbNo");

            migrationBuilder.CreateIndex(
                name: "DATSRCLN_SRC_FK",
                table: "datsrcln",
                column: "DataSrc_ID");

            migrationBuilder.CreateIndex(
                name: "DATSRCLN_NUT_FK",
                table: "datsrcln",
                column: "Nutr_No");

            migrationBuilder.CreateIndex(
                name: "FOOD_DES_FK",
                table: "food_des",
                column: "FdGrp_Cd");

            migrationBuilder.CreateIndex(
                name: "FOOD_DES_UK",
                table: "food_des",
                columns: new[] { "NDB_No", "FdGrp_Cd" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "LANGUAL_LANGDESC_FK",
                table: "langual",
                column: "Factor_Code");

            migrationBuilder.CreateIndex(
                name: "NUT_DATA_NUTDEF_FK",
                table: "nut_data",
                column: "Nutr_No");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomDishItem");

            migrationBuilder.DropTable(
                name: "datsrcln");

            migrationBuilder.DropTable(
                name: "deriv_cd");

            migrationBuilder.DropTable(
                name: "footnote");

            migrationBuilder.DropTable(
                name: "langual");

            migrationBuilder.DropTable(
                name: "nut_data");

            migrationBuilder.DropTable(
                name: "src_cd");

            migrationBuilder.DropTable(
                name: "weight");

            migrationBuilder.DropTable(
                name: "CustomDishes");

            migrationBuilder.DropTable(
                name: "data_src");

            migrationBuilder.DropTable(
                name: "langdesc");

            migrationBuilder.DropTable(
                name: "nutr_def");

            migrationBuilder.DropTable(
                name: "food_des");

            migrationBuilder.DropTable(
                name: "fd_group");
        }
    }
}
