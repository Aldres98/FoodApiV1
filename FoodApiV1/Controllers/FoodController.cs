﻿using System;
using System.Collections.Generic;
using System.Linq;
using FoodApiV1.Models;
using FoodApiV1.Models2;
using FoodApiV1.number2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodApiV1.Controllers
{
    /// <summary>
    /// Controller class for retrieving data from initial usdanlsr28 database
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class FoodController : Controller
    {
        private FoodDbContext db;

        public FoodController(FoodDbContext context)
        {
            db = context;
        }


        /// <summary>
        /// Get all products (Most importantly - descriptions aka names) from database
        /// </summary>
        /// <returns>
        /// List of products with all info from food_des table:
        /// NDB_No, FdGrp_Cd, Long_Desc, Shrt_Desc, ComName, ManufacName e.t.c.
        /// and array of nutrients in readable form
        /// </returns>
        /// <param name="page">Number of page (each page contains 15 results)</param>
        [HttpGet("getFoodDes")]
        public List<FoodDesFormatted> GetFoodDes(int page)
        {

            var foodDescriptions = db.FoodDes
                .Select(food => new FoodDesFormatted
                {
                    FoodDes = new FoodDes
                        {NdbNo = food.NdbNo, FdGrpCd = food.FdGrpCd, ShrtDesc = food.ShrtDesc, LongDesc = food.LongDesc}
                })
                .Skip((page - 1) * 15)
                .Take(15)
                .ToList();
            foreach (var food in foodDescriptions)
            {
                food.Nutrients = GetNutDataByNDB(food.FoodDes.NdbNo);
            }

            return foodDescriptions;
        }

        /// <summary>
        /// Get key-value pairs (Nutrient name - Nutrient value) for a product by its Ndb_No 
        /// </summary>
        /// <param name="NDB_No">NDB_No (ID) of the product</param>
        /// <returns></returns>
        [HttpGet("getNutDataByNDB")]
        public List<ReadableNutrient> GetNutDataByNDB(string NDB_No)
        {
            var nutrientValues = db.NutData.Where(data => data.NdbNo == NDB_No)
                .Join(db.NutrDef, nutData => nutData.NutrNo,
                    nutrDef => nutrDef.NutrNo,
                    (val, name) => new ReadableNutrient {NutrientName = name.NutrDesc, NutrientValue = val.NutrVal})
                .ToList();
            return nutrientValues;
        }


        /// <summary>
        /// Get list of all available food categories
        /// </summary>
        /// <returns></returns>
        [HttpGet("getCategoriesList")]
        public List<FdGroup> GetCategoriesList()
        {
            return db.FdGroup.ToList();
        }

        /// <summary>
        /// Get list of foods related to some category by food group ID
        /// </summary>
        /// <param name="fdGrp">ID of food group</param>
        /// <param name="page">Number of page (each page contains 15 results)</param>
        /// <returns></returns>
        [HttpGet("getFoodsByCategory")]
        public List<FoodDesFormatted> GetFoodsByCategory(string fdGrp, int page)
        {
            var foodDescriptions = db.FoodDes
                .Where(food => food.FdGrpCd == fdGrp)
                .Select(food => new FoodDesFormatted
                {
                    FoodDes = new FoodDes
                        {NdbNo = food.NdbNo, FdGrpCd = food.FdGrpCd, ShrtDesc = food.ShrtDesc, LongDesc = food.LongDesc}
                })
                .Skip((page - 1) * 15)
                .Take(15)
                .ToList();
            foreach (var food in foodDescriptions)
            {
                food.Nutrients = GetNutDataByNDB(food.FoodDes.NdbNo);
            }

            return foodDescriptions;
        }

        /// <summary>
        /// Find food by user's search input
        /// </summary>
        /// <param name="searchQuery">User's search input</param>
        /// <param name="page">Number of page (each page contains 15 results)</param>
        /// <returns></returns>
        [HttpGet("FindFood")]
        public List<FoodDesFormatted> FindFood(string searchQuery, int page)
        {
            var foodDescriptions = db.FoodDes
                .Where(food => food.LongDesc.Contains(searchQuery))
                .Select(food => new FoodDesFormatted
                {
                    FoodDes = new FoodDes
                        {NdbNo = food.NdbNo, FdGrpCd = food.FdGrpCd, ShrtDesc = food.ShrtDesc, LongDesc = food.LongDesc}
                })
                .Skip((page - 1) * 15)
                .Take(15)
                .ToList();
            foreach (var food in foodDescriptions)
            {
                food.Nutrients = GetNutDataByNDB(food.FoodDes.NdbNo);
            }

            return foodDescriptions;
        }

        /// <summary>
        /// Get list of all diets
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDiets")]
        public List<PreparedDiets> GetDiets()
        {
            return db.PreparedDiets.Select(diet =>
                new PreparedDiets
                    {DietDescription = diet.DietDescription, DietId = diet.DietId, DietName = diet.DietName}).ToList();
        }


        /// <summary>
        /// Get list of all prepared dishes
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetPreparedDishes")]
        public List<PreparedDishes> GetPreparedDishes()
        {
            return db.PreparedDishes.Select(dish =>
                new PreparedDishes
                {
                    Carbs = dish.Carbs, Directions = dish.Directions, Fat = dish.Fat,
                    DishDescription = dish.DishDescription, DishId = dish.DishId, DishName = dish.DishName,
                    IngredientList = dish.IngredientList, Kcal = dish.Kcal, PicsUrl = dish.PicsUrl,
                    Protein = dish.Protein
                }).ToList();
        }

        /// <summary>
        /// Get all the data about diets and their diet plans
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetWholeDiets")]
        public List<FormattedDiet> GetWholeDiets()
        {
            var diets = db.PreparedDiets.Select(diet => new PreparedDiets
            {
                DietName = diet.DietName, Preview = diet.Preview, DietDescription = diet.DietDescription,
                DietId = diet.DietId
            }).ToList();
            List<FormattedDiet> formattedDiets = new List<FormattedDiet>();

            foreach (var diet in diets)
            {
                List<FormattedDietPlan> formattedDietPlan = new List<FormattedDietPlan>();
                var dietPlanEntries = db.DietPlan.Where(plan => plan.DietId == diet.DietId).Select(plan => new DietPlan{DishId = plan.DishId, Meal = plan.Meal, DayNumber = plan.DayNumber})
                    .ToList();
                foreach (var entry in dietPlanEntries)
                {
                    var dish = db.PreparedDishes.Where(dish => dish.DishId == entry.DishId)
                        .Select(dish => new FormattedDish
                        {
                            Carbs = dish.Carbs,
                            Directions = dish.Directions,
                            DishDescription = dish.DishDescription,
                            DishId = dish.DishId,
                            DishName = dish.DishName,
                            Fat = dish.Fat,
                            IngredientsList = dish.IngredientList,
                            Kcal = dish.Kcal,
                            PicsUrl = dish.PicsUrl,
                            Protein = dish.Protein
                        }).FirstOrDefault();
                    formattedDietPlan.Add(new FormattedDietPlan{DayNumber = entry.DayNumber, Meal = entry.Meal, Dish = dish});
                }
                formattedDiets.Add(new FormattedDiet{DietId = diet.DietId, DietName = diet.DietName, Preview = diet.Preview, DietDescription = diet.DietDescription, DietPlan = formattedDietPlan});
            }

            return formattedDiets;
        }
    }
}
